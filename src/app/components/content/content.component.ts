import { Component, EventEmitter, Output } from "@angular/core";
import { TasksService } from "src/app/shared/tasks.service";

@Component({
    selector: 'app-content',
    templateUrl: './content.component.html',
    styleUrls: ['./content.component.scss']
})

export class Content {
    constructor(public taskService: TasksService){}

    onChange(id: string) {
        this.taskService.toggleActiveStatus(id);
    }

    deleteTask (id: string) {
        this.taskService.deleteTask(id);
    }
    
};
