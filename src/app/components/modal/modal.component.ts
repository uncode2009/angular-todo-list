import { Component } from "@angular/core";
import { TasksService } from "src/app/shared/tasks.service";

@Component({
    selector:'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})

export class Modal {
    constructor(public tasksService: TasksService){}

    task = 'New task'
    hours = '00'
    minutes = '00'
    meridiem = 'AM'

    onClick(event: any): void {
        event.stopPropagation();
    }

    onAdd() {
        this.tasksService.addTask({
            ID: Date.now().toString(),
            Task: this.task,
            Time: `${this.hours}:${this.minutes} ${this.meridiem}`,
            Active: true
        })
    }

}