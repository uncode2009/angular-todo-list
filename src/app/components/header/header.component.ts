import { Component, Input } from "@angular/core";

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})

export class Header {
    constructor() {
        this.count = 0
    }
    @Input() count: number

}