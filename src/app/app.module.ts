import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { Content } from './components/content/content.component';
import { Header } from './components/header/header.component';
import { Modal } from './components/modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    Header,
    Modal,
    Content
  ],
  imports: [
    BrowserModule,
    HttpClientModule, 
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
