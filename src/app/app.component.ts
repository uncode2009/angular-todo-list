import { Component, OnInit } from '@angular/core';
import { ApiService } from './shared/api.services';
import { TasksService } from './shared/tasks.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor( public tasksService: TasksService) {}
  title = 'todo-list';

  ngOnInit(): void {
    this.tasksService.getTasks();
  };  
}
