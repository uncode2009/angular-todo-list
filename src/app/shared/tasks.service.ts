import { Injectable } from "@angular/core";
import { ApiService } from "./api.services";

export interface ITask {
    ID: string,
    Task: string,
    Time: string,
    Active: boolean
}

@Injectable({providedIn: 'root'})
export class TasksService {
    constructor(private apiService: ApiService) {}
    public tasks: ITask[] = [];
    showModal: boolean = false;
    
    getTasks () {
        this.apiService.getTasks().subscribe(response => {
            this.tasks = response;
        })
    };

    toggleActiveStatus = (id: string) => {
        this.apiService.updateTask(id).subscribe(response => {
            if(response) {
                const idx = this.tasks.findIndex(t => t.ID === id)
                this.tasks[idx].Active = !this.tasks[idx].Active;
            }
        })
    }

    deleteTask(id: string) {
        this.apiService.deleteTask(id).subscribe(response => {
            if (response.message == "Deleted!") {
                this.tasks = this.tasks.filter(t => t.ID !== id)
            }
        })
    }
    
    addTask(task: ITask) {
        this.apiService.createTask(task).subscribe(response => {
            if(response) {
                this.tasks.push(task)
                this.closeModal()
            }
        })
    }
 
    openModal() {
      this.showModal = true;
    };
  
    closeModal() {
      this.showModal = false;
    };
}