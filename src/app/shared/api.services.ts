import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { ITask } from "./tasks.service";

@Injectable({
    providedIn: 'root'
})

export class ApiService {
    constructor(private http: HttpClient) {}

    getTasks(): Observable<ITask[]> {
        return this.http.get<ITask[]>('http://localhost:9090/tasks');
    };

    createTask(newTask: ITask): Observable<ITask> {
        return this.http.post<ITask>('http://localhost:9090/tasks', JSON.stringify(newTask));
    };

    updateTask(id: string): Observable<ITask> {
        return this.http.get<ITask>('http://localhost:9090/tasks/' + id);
    };

    deleteTask(id: string): Observable<{message: string}> {
        return this.http.delete<{message: string}>('http://localhost:9090/tasks/' + id);
    }
}